## NuPack

NuPack is java based CLI tool for quickly computing an estimate of the packaging cost depending up various factors contributing the cost like the base price, number of people needed for packing and the type of package. 

##### System Requirements

1. JAVA JDK (verified with java 8) and JAVA_HOME should be set appropriately

##### Build and Run Instructions

NuPack is gradle based project. However you don't need to have gradle on your system as the wrapper will fetch it for you.

Clone the repo

> git clone https://anoophallur@bitbucket.org/anoophallur/nupack.git

and go to the root of the project

> cd nupack

Execute the gradle wrapper to download gradle

> ./gradlew OR ./gradlew.bat (if you are on an windows system)

Build the project and run the tests and generate IDE files

> ./gradlew idea
> ./gradlew jar
> ./gradlew test

The built jar should be generated at ./build/libs/NuPack.jar

Execute the project from command line as 

> java -jar ./build/libs/Nupack.jar

You can also run the project from IntelliJ if you choose Open Project -> build.gradle of this projct. After navigating to the Main.java class, right click and select run Main.main()


````
[hallur] ~/Workspace/NuPack (git)-[master] % java -jar build/libs/NuPack.jar
Input: $1,299.99, 3 people, food
Output: $1,591.58
Input: $5,432.00, 1 person, drugs
Output: $6,199.81
Input: $12,456.95, 4 people, books
Output: $13,707.63
Input: Wrong input
nupack.io.InputFormatException: Input values shoud be ordered and comma space separated values ex: "price, people, category"
	at nupack.io.InputParser.parse(InputParser.java:20)
	at nupack.cli.Main.main(Main.java:31)
Input:
````

##### Notes

1. All the classes and their responsibilites are documented inline
2. This assumes that the input is correctly formatted. i.e it wont handle correctly where the input is jumbled up, for ex: `3 people, $1,299.99, food` 