package nupack.core;

/**
 * Created by Anoop Hallur on 2/11/17.
 */
// Collections of all the markup values in use currently
public class MarkupConfig {
    public final static double DRUGS_MARKUP = 0.075;
    public final static double FOOD_MARKUP = 0.13;
    public final static double ELECTRONICS_MARKUP = 0.02;
    public final static double GENERIC_MARKUP = 0d;
    public final static double FLAT_MARUP = 0.05;
    public final static double MARKUP_PER_PERSON = 0.012;
}
