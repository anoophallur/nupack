package nupack.core;

import nupack.io.InputModel;

/**
 * Created by Anoop Hallur on 2/11/17.
 */
// Responsible for computing the total markup and the total cost
public class Markup {
    private Double flatMarkupCost = 0d;
    private Double cost = 0d;
    private InputModel inputModel;
    private boolean flatMarkupApplied;
    private boolean peopleMarkupApplied;
    private boolean categoryMarkupApplied;

    /**
     * Saves the input model for computation
     * @param inputModel
     */
    public Markup(InputModel inputModel) {
        this.inputModel = inputModel;
        this.cost = inputModel.getBasePrice();
        flatMarkupApplied = false;
        peopleMarkupApplied = false;
        categoryMarkupApplied = false;
    }

    /**
     * Computes the total cost for the given input model
     * @return
     */
    public Double compute(){
        applyFlatMarkup();
        applyPeopleMarkup();
        applyCategoryMarkup();
        return cost;
    }

    private void applyFlatMarkup(){
        if(!flatMarkupApplied){
            flatMarkupCost = inputModel.getBasePrice() +
                    new FlatMarkup().compute(inputModel.getBasePrice());
            cost = flatMarkupCost;

            flatMarkupApplied = true;
        }
        return;
    }

    private void applyPeopleMarkup(){
        if(!flatMarkupApplied)
            applyFlatMarkup();

        if(!peopleMarkupApplied) {
            cost += new PeopleMarkup().compute(flatMarkupCost, inputModel.getNumberOfPeople());
            peopleMarkupApplied = true;
        }
        return;
    }

    private void applyCategoryMarkup() {
        if (!flatMarkupApplied)
            applyFlatMarkup();

        if (!categoryMarkupApplied) {
            cost += new CategoryMarkup().compute(flatMarkupCost, inputModel.getCategory());
            categoryMarkupApplied = true;
        }
        return;
    }
}
