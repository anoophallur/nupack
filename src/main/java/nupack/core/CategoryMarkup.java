package nupack.core;

/**
 * Created by Anoop Hallur on 2/11/17.
 */
// Responsible for computing the markup associated with the given category
public class CategoryMarkup {

    /**
     * Performs the lookup of the category string and returns the markup percentage
     * @param category
     * @return
     */
    private Double categoryLookup(String category) {
        if(category.contains("drugs") || category.contains("pharmaceuticals"))
            return MarkupConfig.DRUGS_MARKUP;
        else if(category.contains("food"))
            return MarkupConfig.FOOD_MARKUP;
        else if(category.contains("electronics"))
            return MarkupConfig.ELECTRONICS_MARKUP;
        else
            return MarkupConfig.GENERIC_MARKUP;
    }

    /**
     * Applies the categroy markup
     * @param preMarkup
     * @param category
     * @return
     */
    public Double compute(Double preMarkup, String category){
        return preMarkup * categoryLookup(category);
    }
}
