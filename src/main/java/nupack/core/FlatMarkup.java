package nupack.core;

/**
 * Created by Anoop Hallur on 2/11/17.
 */
// Responsible for computing the flat markup
public class FlatMarkup {

    /**
     * Applies the flat markup
     * @param preMarkup
     * @return
     */
    public Double compute(Double preMarkup){
        return preMarkup * MarkupConfig.FLAT_MARUP;
    }
}
