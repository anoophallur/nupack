package nupack.core;

/**
 * Created by Anoop Hallur on 2/11/17.
 */
// Responsible for computing the markup depending on the number of people
public class PeopleMarkup {

    /**
     * Computes the people markup
     * @param preMarkup
     * @param numberOfPeople
     * @return
     */
    public Double compute(Double preMarkup, Integer numberOfPeople){
        return preMarkup * MarkupConfig.MARKUP_PER_PERSON * numberOfPeople;
    }
}
