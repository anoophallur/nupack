package nupack.cli;

import nupack.core.Markup;
import nupack.io.InputModel;
import nupack.io.InputParser;
import nupack.io.OutputFormatter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Anoop Hallur on 2/11/17.
 */

// Responsible for executing the runtime loop while running in CLI mode
public class Main {
    /**
     * Main class for the cli
     * @param args : ignored
     */
    public static void main(String[] args) {
        while (true) {
            BufferedReader br = null;
            try {
                br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Input: ");
                String input = br.readLine();

                InputModel inputModel = new InputParser().parse(input);
                Double cost = new Markup(inputModel).compute();
                String output = new OutputFormatter().format(cost);

                System.out.println("Output: " + output);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
