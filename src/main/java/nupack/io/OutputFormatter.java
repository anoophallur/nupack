package nupack.io;

import java.text.DecimalFormat;

/**
 * Created by Anoop Hallur on 2/11/17.
 */
// Converting the result into the format needed for output display
public class OutputFormatter {

    /**
     * Output format for display
     * @param cost
     * @return
     */
    public String format(Double cost){
        DecimalFormat decimalFormat = new DecimalFormat("$#,###.00");
        return decimalFormat.format(cost);
    }
}
