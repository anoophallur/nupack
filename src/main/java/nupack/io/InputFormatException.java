package nupack.io;

/**
 * Created by Anoop Hallur on 2/11/17.
 */

// Custom exception thrown if user enters input in invalid format
public class InputFormatException extends RuntimeException {

    public InputFormatException(String message) {
        super(message);
    }
}
