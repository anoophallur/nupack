package nupack.io;

/**
 * Created by Anoop Hallur on 2/11/17.
 */

// DAO for input model
public class InputModel {
    private Double basePrice;
    private Integer numberOfPeople;
    private String category;

    public InputModel() {
    }

    public InputModel(Double basePrice, Integer numberOfPeople, String category) {
        this.basePrice = basePrice;
        this.numberOfPeople = numberOfPeople;
        this.category = category;
    }

    public Double getBasePrice() {
        return basePrice;
    }

    public Integer getNumberOfPeople() {
        return numberOfPeople;
    }

    public String getCategory() {
        return category;
    }

    public void setBasePrice(Double basePrice) {
        this.basePrice = basePrice;
    }

    public void setNumberOfPeople(Integer numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
