package nupack.io;

/**
 * Created by Anoop Hallur on 2/11/17.
 */

// Class to parse the input string into the input model
public class InputParser {

    /**
     * Parses the raw input string to generate the input model
     * @param inputString
     * @return
     * @throws InputFormatException
     */
    public InputModel parse(String inputString) throws InputFormatException {
        InputModel inputModel = new InputModel();
        String[] splits = inputString.split(", ");
        if(splits.length != 3)
            throw new InputFormatException("Input values shoud be ordered and comma " +
                    "space separated values ex: \"price, people, category\"");
        try{
            inputModel.setBasePrice(parseBasePrice(splits[0]));
            inputModel.setNumberOfPeople(parseNumberOfPeople(splits[1]));
            inputModel.setCategory(parseCategory(splits[2]));
        } catch (Exception e){
            throw new InputFormatException("Unknown exception while parsing inputs");
        }
        return inputModel;
    }

    /**
     * Parse the category string
     * @param categoryString
     * @return
     */
    public String parseCategory(String categoryString) {
        return categoryString.trim();
    }

    /**
     * Parse the number of people. It should have "people" or "person(s)" in the string
     * @param peopleString
     * @return
     * @throws InputFormatException
     */
    public Integer parseNumberOfPeople(String peopleString) throws InputFormatException {
        if((peopleString.contains("people") || peopleString.contains("person"))){
            String peopleStringFormatted = peopleString.replaceAll("[^\\d]+", "");
            return Integer.parseInt(peopleStringFormatted);
        } else {
            throw new InputFormatException("Number of people should be of the format \"n person\" or \"n people\"");
        }
    }

    /**
     * Parse the category of the package
     * @param priceString
     * @return
     */
    public Double parseBasePrice(String priceString) {
        String priceStringFormatted = priceString.replaceAll("[^\\d.]+", "");
        return Double.parseDouble(priceStringFormatted);
    }
}
