package nupack.core;

import nupack.core.FlatMarkup;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Anoop Hallur on 2/11/17.
 */
public class FlatMarkupTest {

    @Test
    public void checkMarkupApplication(){
        FlatMarkup flatMarkup = new FlatMarkup();
        assertEquals(5d, flatMarkup.compute(100d), 0.000001);
    }
}