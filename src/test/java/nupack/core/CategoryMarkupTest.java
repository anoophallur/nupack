package nupack.core;

import nupack.core.CategoryMarkup;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Anoop Hallur on 2/11/17.
 */
public class CategoryMarkupTest {

    @Test
    public void checkMarkupApplication(){
        CategoryMarkup categoryMarkup = new CategoryMarkup();
        assertEquals(7.5d, categoryMarkup.compute(100d, "drugs"), 0.000001);
        assertEquals(7.5d, categoryMarkup.compute(100d, "pharmaceuticals"), 0.000001);
        assertEquals(13d, categoryMarkup.compute(100d, "food"), 0.000001);
        assertEquals(2d, categoryMarkup.compute(100d, "electronics"), 0.000001);
        assertEquals(0d, categoryMarkup.compute(100d, "toys"), 0.000001);
    }
}