package nupack.core;

import nupack.core.PeopleMarkup;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Anoop Hallur on 2/11/17.
 */
public class PeopleMarkupTest {

    @Test
    public void checkMarkupApplication(){
        PeopleMarkup peopleMarkup = new PeopleMarkup();
        assertEquals(1.2d, peopleMarkup.compute(100d, 1), 0.000001);
        assertEquals(2.4d, peopleMarkup.compute(100d, 2), 0.000001);
        assertEquals(0d, peopleMarkup.compute(100d, 0), 0.000001);
        assertEquals(12d, peopleMarkup.compute(100d, 10), 0.000001);
    }
}