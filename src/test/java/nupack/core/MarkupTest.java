package nupack.core;

import nupack.core.Markup;
import nupack.io.InputModel;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Anoop Hallur on 2/11/17.
 */
public class MarkupTest {

    @Test
    public void checkMarkupComputation(){
        InputModel inputModel1 = new InputModel(1299.99, 3, "food");
        Markup markup1 = new Markup(inputModel1);
        assertEquals(1591.58, markup1.compute(), 0.01);

        InputModel inputModel2 = new InputModel(5432.00, 1, "drugs");
        Markup markup2 = new Markup(inputModel2);
        assertEquals(6199.81, markup2.compute(), 0.01);

        InputModel inputModel3 = new InputModel(12456.95, 4, "books");
        Markup markup3 = new Markup(inputModel3);
        assertEquals(13707.63, markup3.compute(), 0.01);
    }
}