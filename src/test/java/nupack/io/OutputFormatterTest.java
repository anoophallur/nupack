package nupack.io;

import nupack.io.OutputFormatter;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Anoop Hallur on 2/11/17.
 */
public class OutputFormatterTest {
    @Test
    public void shouldFormatAsPerOutput(){
        OutputFormatter outputFormatter = new OutputFormatter();
        assertEquals("$1,591.58" ,outputFormatter.format(1591.5777));
        assertEquals("$1,234,591.50" ,outputFormatter.format(1234591.5));
        assertEquals("$1.00" ,outputFormatter.format(1d));
    }

}