package nupack.io;

import nupack.io.InputFormatException;
import nupack.io.InputModel;
import nupack.io.InputParser;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Anoop Hallur on 2/11/17.
 */
public class InputParserTest {

    @Test
    public void shouldParseCorrectInput() throws InputFormatException {
        InputParser inputParser = new InputParser();
        InputModel inputModel = inputParser.parse("$1,299.99, 3 people, food");
        assertEquals(1299.99, inputModel.getBasePrice(), 0.000001);
        assertEquals(3, inputModel.getNumberOfPeople(), 0);
        assertEquals("food", inputModel.getCategory());
    }

    @Test
    public void shouldParseBasePrice() throws InputFormatException {
        InputParser inputParser = new InputParser();
        Double basePrice = inputParser.parseBasePrice("$1,299.99");
        assertEquals(1299.99, basePrice, 0.000001);
    }

    @Test
    public void shouldParsePeopleString() throws InputFormatException {
        InputParser inputParser = new InputParser();
        Integer numberOfPeople = inputParser.parseNumberOfPeople("1 person");
        assertEquals(1, numberOfPeople, 0);

        Integer moreNumberOfPeople = inputParser.parseNumberOfPeople("3 people");
        assertEquals(3, moreNumberOfPeople, 0);
    }

    @Test
    public void shouldParseCategoryString() throws Exception {
        InputParser inputParser = new InputParser();
        String category = inputParser.parseCategory("food");
        assertEquals("Category not parsed Correctly", "food", category);

    }

    @Test(expected = InputFormatException.class)
    public void shouldThrowExceptionsOnBadInputs(){
        InputParser inputParser = new InputParser();
        inputParser.parse("random");
    }
}